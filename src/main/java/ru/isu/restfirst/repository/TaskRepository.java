package ru.isu.restfirst.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;
import ru.isu.restfirst.model.Task;

@Repository
@CrossOrigin(origins = "*")
public interface TaskRepository extends JpaRepository<Task, Long> {
    @Query("SELECT t FROM Task AS t WHERE t.user.id=:user_id")
    List<Task> findTasksByUser(@Param("user_id") Long user_id);
}
