package ru.isu.restfirst.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;
import ru.isu.restfirst.model.AutoUser;


@Repository
public interface AutoUserRepository extends JpaRepository<AutoUser, Long> {

    public AutoUser findByUsername(String username);
    
    boolean existsByUsername(String username);
}
