/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ru.isu.restfirst.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "task")
public class Task {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "description", nullable = false)
    private String description;
    
    @Column(name = "file_name")
    private String file_name;
    
    @Column(name = "mime_type")
    private String mime_type;
    
    @ManyToOne
    @JoinColumn(name = "user_id")
    private AutoUser user;
    
    public Long getId() {
        return id;
    }
    
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public String getFileName() {
        return file_name;
    }

    public void setFilePath(String file_name) {
        this.file_name = file_name;
    }

    public String getMimeType() {
        return mime_type;
    }
        
    public void mimeType(String mime_type) {
        this.mime_type = mime_type;
    }
    
    // требуется конструктор по умолчанию, так как определён собственный далее
    public Task(){        
    }
    
    public Task(String title, String description, String file_name, String mime_type, AutoUser user) {
        this.title = title;
        this.description = description;
        this.file_name = file_name;
        this.mime_type = mime_type;
        this.user = user;
    }
}