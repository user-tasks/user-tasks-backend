package ru.isu.restfirst.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class TaskResponse {
    private Task task;
    private String file;
    
    public TaskResponse(){        
    }
    
    public TaskResponse(Task task, String file) {
        this.task = task;
        this.file = file;
    }
}
