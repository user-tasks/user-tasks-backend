
package ru.isu.restfirst.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.Base64;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import ru.isu.restfirst.model.AutoUser;
import ru.isu.restfirst.model.Task;
import ru.isu.restfirst.model.TaskRequest;
import ru.isu.restfirst.model.TaskResponse;
import ru.isu.restfirst.repository.AutoUserRepository;
import ru.isu.restfirst.repository.TaskRepository;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins="http://localhost:4200")
public class TaskController {

    @Autowired
    private TaskRepository taskRepository;
    
    @Autowired
    AutoUserRepository userRepository;
    
    @Autowired
    private HttpServletRequest request;
    
    @Autowired
    private ResourceLoader resourceLoader;

    
    @Autowired
    ServletContext context;
    
    @Value("${file.upload.directory}")
    private String uploadDirectory;

    @GetMapping("/tasks")
    public ResponseEntity<List<Task>> getAll() {
        List<Task> tasks = taskRepository.findAll();
        return ResponseEntity.ok(tasks);
    }

    
    @GetMapping("/tasks/userId={userId}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<List<Task>> getAllByUser(@PathVariable("userId") Long user_id) {
        System.out.println(user_id);
        List<Task> tasks = taskRepository.findTasksByUser(user_id);
        System.out.println(tasks.size());
        return ResponseEntity.ok(tasks);
    }
    
    @GetMapping("/task/{taskId}")
    public ResponseEntity<Task> getTaskById(@PathVariable Long taskId) {
        Task task = taskRepository.findById(taskId).orElse(null);
        if (task == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(task);
    }
    
    @GetMapping("/taskFile/{taskId}")
    public ResponseEntity<TaskResponse> getFileByTaskId(@PathVariable Long taskId) throws IOException {
        Task task = taskRepository.findById(taskId).orElse(null);
        if (task == null) {
            return ResponseEntity.notFound().build();
        }

        String fileName = task.getFileName();
        String filePath = uploadDirectory + "/" + fileName;

        File file = new File(filePath);
        if (!file.exists()) {
            return ResponseEntity.notFound().build();
        }

        // кодируем файл в формат base64
        byte[] fileContent = Files.readAllBytes(Path.of(filePath));
        String base64File = Base64.getEncoder().encodeToString(fileContent);
    
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fileName);
        
        TaskResponse taskResponse = new TaskResponse(task, base64File);
        return ResponseEntity.ok(taskResponse);
    }

    @PostMapping(value = "/addTask", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<Task> createTask(
            @RequestParam String title, 
            @RequestParam String description, 
            @RequestParam Long user_id, 
            @RequestParam MultipartFile file
    ) throws IOException {

        AutoUser user = userRepository.getOne(user_id);
        
        String fileName = file.getOriginalFilename();
        
        Path filePath = Path.of(uploadDirectory, fileName);
    
        Files.copy(file.getInputStream(), filePath, StandardCopyOption.REPLACE_EXISTING);
        
        String mimeType = file.getContentType();

        Task createdTask = new Task(title, description, fileName, mimeType, user);
        taskRepository.save(createdTask);
        return ResponseEntity.ok(createdTask);
    }

//    @PutMapping("/{taskId}")
    @PostMapping("/updateTask/{taskId}")
    public ResponseEntity<Task> updateTask(@PathVariable Long taskId, @RequestBody TaskRequest taskRequest) {
        Task updatedTask = taskRepository.findById(taskId).orElse(null);
        if (updatedTask == null) {
            return ResponseEntity.notFound().build();
        }
        updatedTask.setTitle(taskRequest.getTitle());
        updatedTask.setDescription(taskRequest.getDescription());
        taskRepository.save(updatedTask);
        return ResponseEntity.ok(updatedTask);
    }

//    @DeleteMapping("/{taskId}")
    @PostMapping("/deleteTask/{taskId}")
    public ResponseEntity<?> deleteTask(@PathVariable Long taskId) {
        if (taskRepository.existsById(taskId)) {
            taskRepository.deleteById(taskId);
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.notFound().build();
    }

}
